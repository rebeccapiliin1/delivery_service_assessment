<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerPolicies();
        Passport::tokensCan([
            'create' => 'can create origin',
            'read' => 'can read origin',
            'update' => 'can edit origin',
            'delete' => 'can delete origin',
            'routes-create' => 'can create origin',
            'routes-read' => 'can read origin',
            'routes-update' => 'can edit origin',
            'routes-delete' => 'can delete origin',
            'shortest_path-read' => 'can use shortest path',
            'users-create' => 'can create user',
            'users-read' => 'can read user',
            'users-update' => 'can update user',
            'users-delete' => 'can delete users',
            'user_roles-read' => 'can read permissions'
        ]);
        Passport::routes();
    }
}
