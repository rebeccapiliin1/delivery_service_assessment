<?php
    namespace App\CustomLibrary;

    class Dijkstra {

        private $objGraph;
        
        public function __construct($objGraph)
        {
            $this->objGraph = $objGraph;
        }

        public function findShortestPath($strIndexFrom, $strIndexTo)
        {
            $arPath = [];
            $arVisited = [];
            $arUnvisited = $this->objGraph->getColumns();

            // Prepare ArPath
            foreach($arUnvisited as $strVertex)
            {
                $arPath[$strVertex] = [
                    "previous_vertex" => null,
                    "shortest_path_value" => ["time" => INF, "cost" => INF],
                    "route_id" => 0
                ];
            }

            $arPath[$strIndexFrom]["shortest_path_value"] = ["time" => 0, "cost" => 0];

            $this->dijkstra($arPath, $strIndexTo, $arVisited, $arUnvisited);

            $arResult = [];
            $this->getResult($arResult, $arPath, $strIndexFrom, $strIndexTo);
            
            $objResult = new DijkstraResult($this->cleanResult($arResult, $strIndexTo));

            return $objResult;

        }

        private function cleanResult($arResult, $strIndexTo)
        {
            $result = [];
            for($c = count($arResult)-1; $c >= 0; $c--)
            {
                $currentValue = $arResult[$c];
                if($currentValue["shortest_path_value"]["time"] === INF)
                {
                    return null;
                }
                $result[] = $currentValue;
            }
            return $result;
        }

        private function getResult(&$arResult, $arPath, $strIndexFrom, $strIndexTo)
        {
            $currentRow = $arPath[$strIndexTo];
            array_push($arResult, array_merge(["point_id" => $strIndexTo], $currentRow));

            if(is_null($currentRow["previous_vertex"]))
            {
                return;
            }

            $this->getResult($arResult, $arPath, $strIndexFrom, $currentRow["previous_vertex"]);
        }

        private function dijkstra(&$arPath, $strIndexTo, &$arVisited, &$arUnvisited)
        {
            // Get the Lowest Index from the Temp Path Holder
            $currentIndex = $this->getLowestUnvisited($arPath, $arVisited);

            if(is_null($currentIndex))
            {
                return;
            }

            // Get the Index where we at right now
            $currentValue = $arPath[$currentIndex]; 

            // Get the Current  Cost
            $currentTime = $currentValue["shortest_path_value"]["time"];
            $currentCost = $currentValue["shortest_path_value"]["cost"];

            // Get all the Available Routes
            $arRoutes = $this->objGraph->getArray();

            if(isset($arRoutes[$currentIndex]))
            {
                // Get the neightbors of the current index
                foreach($arRoutes[$currentIndex] as $pointId => $value)
                {
                    // Get the Value of that neighbor int he Temp Path Holder
                    $currentNeighborValue = $arPath[$pointId];

                    // Get the Neighbor  Time Cost
                    $currentNeighborTime = $currentNeighborValue["shortest_path_value"]["time"];
                    $currentNeighborCost = $currentNeighborValue["shortest_path_value"]["cost"];

                    // Get the current Calculation
                    $currentCalculationTime = $currentTime + $value["time"];
                    $currentCalculationCost = $currentCost + $value["cost"];

                    if($currentCalculationTime <= $currentNeighborTime)
                    {
                        $arPath[$pointId]["route_id"] = $value["route_id"];
                        $arPath[$pointId]["shortest_path_value"]["time"] = $currentCalculationTime;
                        $arPath[$pointId]["shortest_path_value"]["cost"] = $currentCalculationCost;
                        $arPath[$pointId]["previous_vertex"] = $currentIndex;
                    }
                        
                }
            }
            else
            {
                return;
            }

            // Mark the current index as visited
            $arVisited[] = $currentIndex;
            $this->dijkstra($arPath, $strIndexTo, $arVisited, $arUnvisited);
        }

        private function getLowestUnvisited($arPath, $arVisited)
        {
            $lowestIndex = null;
            $lowest = ["time" => INF, "cost" => INF];

            foreach($arPath as $id => $row)
            {
                if(!in_array($id, $arVisited))
                {
                    if($row["shortest_path_value"]["time"] <= $lowest["time"])
                    {
                        $lowestIndex = $id;
                        $lowest = $row["shortest_path_value"];
                    }
                }
            }

            return $lowestIndex;
        }
    }

    class DijkstraResult {
        public $arResult;

        public function __construct($arResult)
        {
            $this->arResult = $arResult;
        }
    }