<?php
    namespace App\CustomLibrary;

    class Graph {
    
        private $arColumns;
        private $arData;

        public function __construct($arColumns)
        {
            $this->arData = Array();
            $this->arColumns = $arColumns;
        }

        public function addVertex($strPointA, $strPointB, $intRouteId, $fTime, $fCost, $bIsBidirectional = false)
        {
            $arRow = isset($this->arData[$strPointA]) ? $this->arData[$strPointA] : [];
            foreach($this->arColumns as $strColumn)
            {
                if($strPointB == $strColumn)
                {
                    $arRow[$strPointB] = [
                        "route_id" => $intRouteId,
                        "time" => $fTime,
                        "cost" => $fCost
                    ];
                }
            }
            
            $this->arData[$strPointA] = $arRow;
        }

        public function getArray()
        {
            return $this->arData;
        }

        public function getColumns()
        {
            return $this->arColumns;
        }
    }