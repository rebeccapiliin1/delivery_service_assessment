<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
    public function routes()
    {
        return $this->hasMany('App\Routes', 'from', 'id');
    }
}
