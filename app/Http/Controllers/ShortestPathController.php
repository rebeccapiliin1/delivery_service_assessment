<?php

namespace App\Http\Controllers;

use App\CustomLibrary\Dijkstra;
use App\Routes;
use App\CustomLibrary\Graph;
use App\Http\Resources\FailedResources;
use App\Http\Resources\ShortestPathResources;
use App\Origin;
use Illuminate\Http\Request;

class ShortestPathController extends Controller
{
    public function find(Request $request)
    {
        try
        {
            $validateData = $request->validate([
                'from' => 'required',
                'to' => 'required',
                'search_for' => 'required|in:ID,Name'
            ]);

            $arSearchBy = ["ID" => "id", "Name" => "name"];
            $objFrom = Origin::where($arSearchBy[$validateData["search_for"]], $validateData["from"])->first();
            $objTo = Origin::where($arSearchBy[$validateData["search_for"]], $validateData["to"])->first();

            if(!$objTo && !$objFrom)
            {
                throw new \Exception("Can't find Origin.");
            }

            
            $arColumns = [];
            $arPoints = Origin::all();
            foreach($arPoints as $objPoint)
            {
                $arColumns[] = $objPoint->id;
            }
            $objGraph = new Graph($arColumns);
            $arRoutes = Routes::all();
            foreach($arRoutes as $objRoute)
            {
                $objGraph->addVertex(
                    $objRoute->from, 
                    $objRoute->to,
                    $objRoute->id,
                    $objRoute->time,
                    $objRoute->cost,
                    false
                );
            }

            $objDijsktra = new Dijkstra($objGraph);
            $objResult = $objDijsktra->findShortestPath($objFrom->id, $objTo->id);
            return new ShortestPathResources($objResult);
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }
}
