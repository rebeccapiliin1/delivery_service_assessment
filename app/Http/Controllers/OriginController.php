<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResources;
use App\Http\Resources\OriginResources;
use App\Http\Resources\SuccessResources;
use App\Origin;
use Exception;

use Illuminate\Http\Request;

class OriginController extends Controller
{
    public function index()
{
        $objOrigin = Origin::all();

        return OriginResources::collection($objOrigin);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            if($request->isMethod('put'))
            {
                $validatedInput = $request->validate([
                    'id' => 'required'
                ]);

                $objPoint = Origin::find($validatedInput["id"]);
                if(!$objPoint)
                {
                    throw new \Exception("Can't find id.");
                }
            }
            else
            {
                $validatedInput = $request->validate([
                    "origin_name" => "required"
                ]);

                $objPoint = new Origin();
            }

            $objPoint->point_name = $request->point_name;

            if($objPoint->save())
            {
                return new OriginResources($objPoint);
            }
            
            throw new \Exception("Saving failed.");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }

    public function show(Request $request)
    {
        try
        {
            $validatedInput = $request->validate([
                'get' => 'required',
                'search_for' => 'required|in:ID,Name'
            ]);

            $arSearchBy = ["ID" => "id", "Name" => "origin_name"];

            $objPoint = Origin::where($arSearchBy[$validatedInput["search_for"]], $validatedInput["get"])->first();
            if($objPoint)
            {
                return new OriginResources($objPoint);
            }
            throw new Exception("Can't find that origin.");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request)
    {
        try
        {
            $validatedInput = $request->validate([
                "id" => "required|integer"
            ]);

            $objPoint = Origin::find($validatedInput["id"]);
            if($objPoint)
            {
                $objPoint->delete();
                return new SuccessResources(null);
            }
            
            throw new Exception("Can't find origin with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }
}
