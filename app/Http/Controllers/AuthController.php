<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:20',
            'email' => 'email|required',
            'password' => 'required|confirmed',
            'role_id' => 'required|integer'
        ]);

        $validateData['password'] = bcrypt($request->password);
        $validateData['role_id'] = intval($validateData['role_id']);

        // // Get User Permission
        $role = Role::find($validateData['role_id']);
        if($role)
        {
            $user = User::create($validateData);
            
            $arPriviledges = $role->getScopes();
            $token = $user->createToken('My Token', $arPriviledges)->accessToken;

            return response([
                "user" => $user,
                "token" => $token
            ]);
        }

        return response([
            "success" => false,
            "message" => "Can't find id."
        ]);
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password'=> 'required'
        ]);

        if(!auth()->attempt($loginData))
        {
            return response([
                'success' => false,
                'message' => 'Invalid credentials'
            ]);
        }

        $user = Auth::user();
        $userPermission = Role::find($user->role_id);
        $arPriviledges = $userPermission->getScopes();

        $token = $user->createToken('My Token', $arPriviledges)->accessToken;
        return response([
            "user" => $user,
            "token" => $token
        ]);
    }
}
