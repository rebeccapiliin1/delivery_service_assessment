<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResources;
use App\Http\Resources\RoutesResources;
use App\Http\Resources\SuccessResources;
use App\Routes;
use App\Origin;

use Exception;
use Illuminate\Http\Request;

class RoutesController extends Controller
{
    public function index()
    {
        $arRoutes = Routes::all();

        return RoutesResources::collection($arRoutes);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $validateInput = $request->validate([
                "from" => "required|integer",
                "to" => "required|integer",
                "time" => "required",
                "cost" => "required"
            ]);

            if($request->isMethod('put'))
            {
                $validateInput = $request->validate([
                    "id" => "required"
                ]);

                $objRoutes = Routes::find($request->id);
                if(!$objRoutes)
                {
                    throw new Exception("Can't find a route with that id.");
                }
            }
            else
            {
                $validateInput = $request->validate([
                    "bi_directional" => "required|boolean"
                ]);

                $objRoutes = Routes::where('from', $request->from)
                    ->where('to', $request->to)
                    ->first();
                if(!$objRoutes)
                {
                    $objRoutes = new Routes();
                }

                
                $bkRoutesB = Routes::where('from', $request->to)
                    ->where('to', $request->from)
                    ->first();
                if(!$bkRoutesB)
                {
                    $bkRoutesB = new Routes();
                }
            }

            // Check if exists
            $objFrom = Origin::find($request->from);
            $objTo = Origin::find($request->to);

            if(!$objFrom && !$objTo)
            {
                throw new Exception("Can't find from / to.");
            }

            if(isset($bkRoutesB))
            {
                $bkRoutesB->from = $request->to;
                $bkRoutesB->to = $request->from;
                $bkRoutesB->time = $request->time;
                $bkRoutesB->cost = $request->cost;

                $bkRoutesB->saveOrFail();
            }

            $objRoutes->from = $request->from;
            $objRoutes->to = $request->to;
            $objRoutes->time = $request->time;
            $objRoutes->cost = $request->cost;

            if($objRoutes->save())
            {
                return new RoutesResources($objRoutes);
            }
            throw new Exception("Failed!");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try
        {
            $validateInput = $request->validate([
                "id" => "required|integer"
            ]);

            $objRoutes = Routes::find($validateInput["id"]);
            if($objRoutes)
            {
                return new RoutesResources($objRoutes);
            }
            throw new Exception("Can't find route with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $validateInput = $request->validate([
                "id" => "required|integer"
            ]);

            $objRoutes = Routes::find($validateInput["id"]);
            if($objRoutes)
            {
                $objRoutes->delete();
                return new SuccessResources($objRoutes);
            }
            throw new Exception("Can't find route with that id.");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }
}
