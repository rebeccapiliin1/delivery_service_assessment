<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResources;
use App\Http\Resources\RoleResources;
use App\Role;
use Exception;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return RoleResources::collection($roles);
    }

    public function show(Request $request)
    {
        try
        {
            $validateInput = $request->validate([
                "id" => "required"
            ]);

            $role = Role::find($request->id);
            if($role)
            {
                return new RoleResources($role);
            }
            throw new Exception("Can't find role with that id");
        }
        catch(Exception $e)
        {
            return new FailedResources($e);
        }
    }
}
