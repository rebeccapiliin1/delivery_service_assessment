<?php

namespace App\Http\Controllers;

use App\Http\Resources\FailedResources;
use App\Http\Resources\SuccessResources;
use App\Http\Resources\UserResources;
use App\User;
use App\Role;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $objPoints = User::all();

        return UserResources::collection($objPoints);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validateData = $request->validate([
                'id' => 'required',
                'name' => 'required|max:20',
                'email' => 'email|required',
                'password' => 'required|confirmed',
                'role_id' => 'required|integer'
            ]);
    
            $validateData['password'] = bcrypt($request->password);
            $validateData['role_id'] = intval($validateData['role_id']);
    

            $user = User::find($validateData["id"]);
            $userPermission = Role::find($validateData['role_id']);
            if($userPermission && $user)
            {
                $user->name = $validateData["name"];
                $user->email = $validateData["email"];
                $user->password = $validateData["password"];
                $user->role_id = $validateData["role_id"];
                
                $user->save();
                
                return new UserResources($user);
            }
            throw new Exception("Can't find user / user permission with that id");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
            $validateData = $request->validate([
                'id' => 'required|integer'
            ]);

            $user = User::find($validateData["id"]);
            if($user)
            {
                return new UserResources($user);
            }
            throw new \Exception("Can't find user   .");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $validateData = $request->validate([
                "id" => "required|integer"
            ]);

            $user = User::find($validateData["id"]);
            if($user)
            {
                $user->delete();
                return new SuccessResources(null);
            }
            
            throw new Exception("Can't find origin.");
        }
        catch(\Exception $e)
        {
            return new FailedResources($e);
        }
    }
}
