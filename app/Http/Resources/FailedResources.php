<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FailedResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'success' => false,
            'message' => $this->getMessage()
        ];
    }
    public function with($request)
    {
        return ['code' => 400];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(400, 'Bad Request');
    }
}
