<?php

namespace App\Http\Resources;

use App\Routes;
use App\Origin;
use Illuminate\Http\Resources\Json\JsonResource;

class ShortestPathResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $returnResult = [];
        $overAllCost = 0;
        $overAllTime = 0;

        $arResult = $this->arResult;
        $route_no = 0;
        if(!is_null($arResult))
        {
            foreach($arResult as $rowResult)
            {
                $objPoint = Origin::find($rowResult["point_id"]);
                $objPointConnection = $rowResult["route_id"] ?: new RoutesResources(Routes::find($rowResult["route_id"]));
                $returnResult[] = [
                    "step" => $route_no++,
                    "route_id" => $objPointConnection,
                    "time" => $rowResult["shortest_path_value"]["time"],
                    "cost" => $rowResult["shortest_path_value"]["cost"],
                    "origin" => new OriginResources($objPoint),
                ];

                $overAllCost = $rowResult["shortest_path_value"]["cost"];
                $overAllTime = $rowResult["shortest_path_value"]["time"];
            }

            return [
                "success" => true,
                "routes" => $returnResult,
                "total_time" => $overAllTime,
                "total_cost" => $overAllCost
            ];
        }

        return [
            "success" => false,
            "routes" => null,
            "message" => "There's no path connected for the Points"
        ]; 
    }

    public function with($request)
    {
        return ['code' => 200];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(200, 'OK');
    }
}
