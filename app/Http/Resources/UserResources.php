<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $role = $this->role;
        $role->privileges = $this->role->privileges;
        return [
            "success" => true,
            "id" => $this->id,
            "email" => $this->email,
            "role" => $this->role
        ];
    }

    public function with($request)
    {
        return ['code' => 200];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(200, 'OK');
    }
}
