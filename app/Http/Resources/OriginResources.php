<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OriginResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'origin_name' => $this->origin_name,
            'routes' => RoutesResources::collection($this->routes)
        ];
    }

    public function with($request)
    {
        return ['code' => 200];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(200, 'OK');
    }
}
