<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API RoutesController
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', 'AuthController@register')->middleware(['auth:api', 'scope:users-create']);
Route::post('login', 'AuthController@login');
Route::get('users', 'UserController@index')->middleware(['auth:api', 'scope:users-read']);
Route::get('user', 'UserController@show')->middleware(['auth:api', 'scope:users-read']);
Route::put('user', 'UserController@store')->middleware(['auth:api', 'scope:users-update']);
Route::delete('user', 'UserController@destroy')->middleware(['auth:api', 'scope:users-delete']);
Route::get('points', 'OriginController@index')->middleware(['auth:api', 'scope:read']);
Route::get('origin', 'OriginController@show')->middleware(['auth:api', 'scope:read']);
Route::post('origin', 'OriginController@store')->middleware(['auth:api', 'scope:create']);
Route::put('origin', 'OriginController@store')->middleware(['auth:api', 'scope:update']);;
Route::delete('origin', 'OriginController@destroy')->middleware(['auth:api', 'scope:delete']);;
Route::get('route', 'RoutesController@show')->middleware(['auth:api', 'scope:routes-read']);;
Route::get('routes', 'RoutesController@index')->middleware(['auth:api', 'scope:routes-read']);;
Route::post('route', 'RoutesController@store')->middleware(['auth:api', 'scope:routes-create']);;
Route::put('route', 'RoutesController@store')->middleware(['auth:api', 'scope:routes-update']);;
Route::delete('route', 'RoutesController@destroy')->middleware(['auth:api', 'scope:routes-delete']);;
Route::get('path', 'ShortestPathController@find')->middleware(['auth:api', 'scope:shortest_path-read']);
Route::get('roles', 'RoleController@index')->middleware(['auth:api', 'scope:user_roles-read']);
Route::get('role', 'RoleController@show')->middleware(['auth:api', 'scope:user_roles-read']);
