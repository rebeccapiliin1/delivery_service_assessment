
```
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan passport:install
create local db 'deliveryservice'
create .env file
change db settings
php artisan serve

Route list
-POST-
/api/login
/api/register
/api/origin
/api/route

-get-
/api/user
/api/points
/api/origin
/api/routes
/api/roles

-put-
/api/user
/api/route

delete
/api/user
/api/route

ADMIN can delete, update, create, read
guest can use API
```
