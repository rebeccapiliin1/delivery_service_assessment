<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolePrigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_prig')->insert([
            [
                'role_id' => 1,
                'module_name' => 'origin',
                'create' => true,
                'read' => true,
                'update' => true,
                'delete' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 1,
                'module_name' => 'routes',
                'create' => true,
                'read' => true,
                'update' => true,
                'delete' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 1,
                'module_name' => 'shortest_path',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 2,
                'module_name' => 'shortest_path',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 2,
                'module_name' => 'origin',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 2,
                'module_name' => 'routes',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 1,
                'module_name' => 'users',
                'create' => true,
                'read' => true,
                'update' => true,
                'delete' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'role_id' => 1,
                'module_name' => 'user_role',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
