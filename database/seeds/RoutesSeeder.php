<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Origin;

class RoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routes = [
            [
                'from' => 'A',
                'to' => 'C',
                'time' => 1,
                'cost' => 20,
            ],
            [
                'from' => 'A',
                'to' => 'E',
                'time' => 30,
                'cost' => 5,
            ],
            [
                'from' => 'A',
                'to' => 'H',
                'time' => 10,
                'cost' => 1,
            ],
            [
                'from' => 'H',
                'to' => 'E',
                'time' => 30,
                'cost' => 1,
            ],
            [
                'from' => 'E',
                'to' => 'D',
                'time' => 3,
                'cost' => 5,
            ],
            [
                'from' => 'D',
                'to' => 'F',
                'time' => 4,
                'cost' => 50,
            ],
            [
                'from' => 'F',
                'to' => 'I',
                'time' => 45,
                'cost' => 50,
            ],
            [
                'from' => 'I',
                'to' => 'B',
                'time' => 65,
                'cost' => 5,
            ],
            [
                'from' => 'F',
                'to' => 'G',
                'time' => 40,
                'cost' => 50,
            ],
            [
                'from' => 'G',
                'to' => 'B',
                'time' => 64,
                'cost' => 73,
            ],
            [
                'from' => 'C',
                'to' => 'B',
                'time' => 1,
                'cost' => 12,
            ]
        ];

        foreach($routes as $route){
            $From = Origin::where('name', $route['from'])->first();
            $To = Origin::where('name', $route['to'])->first();

            DB::table('routes')->insert(
                [
                    'from' => $From->id,
                    'to' => $To->id,
                    'time' => $route['time'],
                    'cost' => $route['cost'],
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            );
        }
    }
}
