<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OriginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $letters = range('A','I');
        
        $arInsert = [];
        foreach($letters as $letter)
        {
            $arInsert[] = [
                'origin_name' => $letter,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        DB::table('origin')->insert(
            $arInsert
        );
    }
}
