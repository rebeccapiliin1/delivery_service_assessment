<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OriginSeeder::class);
        // $this->call(RoutesSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(UserRolePrigSeeder::class);
        $this->call(UserSeeder::class);
    }
}
